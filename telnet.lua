local server

return {
  start = function()
    local config = require('config').load().telnet
    if config.enable then
      server = net.createServer(net.TCP, config.timeout)
      server:listen(config.port, function(conn)
                      conn:send("> ")
                      conn:on("receive", function(conn, data)
                                node.output(function(out) conn:send(out) end, 1)
                                -- using pcall loadstring instead of node.input in order to get synchronous execution
                                pcall(loadstring(data)) --node.input(data)
                                node.output(nil)
                                conn:send("> ")
                      end)
      end)
    end
  end,
  stop = function()
    if server ~= nil then
      server:close()
      server = nil
    end
  end
}
