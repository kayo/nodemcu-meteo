local sensor = require 'sensor'

return function(conn, data)
  method, path, proto = data:match("(%u+)%s+([^%s]+)%s+(HTTP/%d.%d)%s+")
  conn:on("sent", function(conn) conn:close() end)
  if method == "GET" then
    if path == "/" then
      conn:send(proto.." 200 OK\r\nContent-Type: application/json\r\n\r\n")
      conn:send(cjson.encode(sensor.result))
    else
      conn:send(proto.."404 Not Found\r\n")
    end
  else
    conn:send(proto.."405 Method Not Allowed\r\n")
  end
end
