local server

local config = require('config').load().server
local handleHTTP = require 'server_http'
local handleTelnet = require 'server_telnet'
--local handleParam = require 'server_param'

return {
  start = function()
    if config.enable then
      server = net.createServer(net.TCP, config.timeout)
      server:listen(config.port, function(conn)
                      local handle
                      conn:on("receive", function(conn, data)
                                if handle == nil then
                                  handle = data:find("%u+%s+[^%s]+%s+HTTP/%d.%d%s+") == 1 and handleHTTP or
                                    data:find("{\"") == 1 and handleParam or handleTelnet
                                end
                                if handle then
                                  handle(conn, data)
                                end
                      end)
      end)
    end
  end,
  stop = function()
    if server ~= nil then
      server:close()
      server = nil
    end
  end
}
