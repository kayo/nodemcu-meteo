local id = 0
local ADDR, SDA, SCL -- buffer device address and pinout

return {
  -- 2 bytes to unsigned int
  b2u = function(MSB, LSB)
    return MSB * 256 + LSB
  end,

  -- 2 bytes to signed int
  b2s = function(MSB, LSB)
    local w = MSB * 256 + LSB
    return (w > 32767) and (w - 65536) or w
  end,
  
  -- 2 bytes to signed int (sign bit)
  b2n = function(MSB, LSB)
    local w = MSB * 256 + LSB
    return (w > 32767) and (32768 - w) or w
  end,

  -- 3 bytes to unsigned long
  b3i = function(MSB, LSB, XLSB)
    return MSB * 65536 + LSB * 256 + XLSB
  end,

  -- initialize i2c
  init = function(addr, sda, scl)
    if (addr and addr ~= ADDR) then
      ADDR = addr
    end
    if (sda and sda ~= SDA) or (scl and scl ~= SCL) then
      SDA, SCL = sda, scl
      i2c.setup(id, SDA, SCL, i2c.SLOW)
    end
  end,
  
  -- wakeup device
  wake = function()
    i2c.start(id)
    i2c.address(id, ADDR, i2c.TRANSMITTER)
    i2c.stop(id)
  end,
  
  -- write data register
  -- ...: register,values to write to the register
  write = function(...)
    i2c.start(id)
    i2c.address(id, ADDR, i2c.TRANSMITTER)
    i2c.write(id, ...)
    i2c.stop(id)
  end,

  -- read data register
  -- len: bytes to read
  read = function(len)
    i2c.start(id)
    i2c.address(id, ADDR, i2c.RECEIVER)
    local c = i2c.read(id, len)
    i2c.stop(id)
    return c
  end,
}
