local timer, status
local cbs = {}
local function run(...)
  for f,v in pairs(cbs) do
    f(...)
  end
end
local function poll(config)
  timer = config.timer
  tmr.alarm(timer, config.interval, 1, function()
              local status_ = wifi.sta.status()
              if status == status_ then return end
              status = status_
              if status == wifi.STA_APNOTFOUND or
                status == wifi.STA_WRONGPWD or
              status == wifi.STA_FAIL then
                if config.mode == "sta||ap" then
                  wifi.setmode(wifi.STATIONAP)
                end
                --disconnected
                run(false)
              elseif status == wifi.STA_IDLE then
                wifi.setmode(wifi.STATION)
                --connected
                run(true)
              end
  end)
end
return {
  register = function(cb)
    cbs[cb] = true
  end,
  unregister = function(cb)
    cbs[cb] = nil
  end,
  start = function()
    local config = require('config').load().radio
    
    if config.ap ~= nil then
      wifi.ap.config(config.ap)
    end
    
    if config.sta ~= nil then
      wifi.sta.config(config.sta.ssid, config.sta.pwd)
    end
    
    if config.mode == "sta||ap" then
      wifi.setmode(wifi.STATION)
      poll(config)
    elseif config.mode == "sta&&ap" then
      wifi.setmode(wifi.STATIONAP)
      poll(config)
    elseif config.mode == "sta" then
      wifi.setmode(wifi.STATION)
      poll(config)
    elseif config.mode == "ap" then
      wifi.setmode(wifi.SOFTAP)
      --connected
      run(true)
    end
  end,
  stop = function()
    if timer ~= nil then
      tmr.stop(timer)
      timer = nil
      --disconnected
      run(false)
    end
  end
}
