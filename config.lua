local filename = "config.json"

return {
  load = function()
    file.open(filename, "r")
    local config = cjson.decode(file.read())
    file.close()
    return config
  end
}
