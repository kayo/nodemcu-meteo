local param = require 'param'

return function(conn, data)
  local req = cjson.decode(data)
  local res
  if req.info then
    res = req.info and param.info[req.info] or param.list
  elseif req.gets then
    res = {}
    for i, k in ipairs(req.gets) do
      table.insert(res, param.gets and param.gets[k] and param.gets[k]())
    end
  elseif req.sets then
    for k, v in pairs(req.sets) do
      if param.sets and param.sets[k] then
        param.sets[k](v)
      end
    end
    res = {}
  end
  if res then
    conn:send(cjson.encode(res))
  end
end
