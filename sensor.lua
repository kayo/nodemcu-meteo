local config = require('config').load().sensor

local drivers = {
  i2c = function(sensor)
    local sda, scl = 4, 3
    sensor.init(sda, scl)
    return sensor.read()
  end
}
local sensors = {}
local result = {}
local timer

local function measure()
  for driver, group in pairs(sensors) do
    for name, sensor in pairs(group) do
      result[name] = drivers[driver](sensor)
    end
  end
end

return {
  start = function()
    local config = require('config').load().sensor
    if config.enable then
      for name, driver in pairs(drivers) do
        if type(config[name]) == 'table' then
          sensors[name] = {}
          for index, sensor in ipairs(config[name]) do
            sensors[name][sensor] = require(sensor)
          end
        end
      end
      timer = config.timer
      tmr.alarm(timer, config.interval, 1, measure)
    end
  end,
  stop = function()
    if timer ~= nil then
      tmr.stop(timer)
      timer = nil
    end
  end,
  result = result
}
