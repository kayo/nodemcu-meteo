return function(conn, data)
  node.output(function(out) conn:send(out) end, 1)
  -- using pcall loadstring instead of node.input in order to get synchronous execution
  pcall(loadstring(data)) --node.input(data)
  node.output(nil)
  conn:send("> ")
end
