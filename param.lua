local sensor = require 'sensor'

return {
  list = { "pressure", "humidity", "temperature" },
  info = {
    pressure = { type = "float", size = 4, unit = "mmHg", readable = true },
    temperature = { type = "float", size = 4, unit = "degC", readable = true },
    humidity = { type = "float", size = 4, unit = "%", readable = true }
  },
  gets = {
    pressure = function()
      return sensor.bmp180.pressure
    end,
    humidity = function()
      return sensor.am2320.humidity
    end,
    temperature = function()
      return sensor.bmp180.temperature and sensor.am2320.temperature and
        (sensor.bmp180.temperature + sensor.am2320.temperature) / 2 or
        sensor.bmp180.temperature or sensor.am2320.temperature
    end
  }
}
